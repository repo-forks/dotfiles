# dotfiles

1) If you are confused by the repository name, then stay far away from this repository. If downloaded, it can confuse you greatly.

2) If you understand the repository name but do not feel comfortable with what is in the repository, approach carefully with a giant stick. If the repository is downloaded, be ready to `rm` at a moment's notice.

3) If you understand the repository name and feel comfortable with what is in the repository, download it as many times as you like. I trust that you and keybindings are on good terms.

4) If you understand the repository name, feel comfortable with what is in the repository, and approve of what is in this repository, download it once. Then like the good and lazy software developer that you are, write a `cron` job to sync your local repository to mine.

5) If you understand the repository name, feel comfortable with what is in the repository, and disapprove of what is in this repository, submit me merge requests. Then we can have flame wars about content and aesthetics that I'll entertain for a day or two before closing your merge request with unmerged commits, either because I actually disagreed with your changes or because I secretly agreed and made your commits as my own. That's not plagiarism. It's open source.
